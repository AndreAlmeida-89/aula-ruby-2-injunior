require 'date'

data_hoje = Date.today
senha_correta = "123"


class Usuario
    attr_accessor :email, :senha, :nome, :nascimento, :logado
    def initialize(email, senha, nome, nascimento, logado)
        @email = email
        @senha = senha
        @nome = nome
        @nascimento = nascimento
        @logado = logado
    end

    #Retorna a idade do usuário, levando em conta a datade nascimento.
    def idade
        (data_hoje - @nascimento).to_i / 365    
    end

    # Modifica a variável logado para true, caso a senha esteja certa.
    def logar (senha)
        if @senha == senha_correta then @logado = true
    end

    #Modifica a variável logado para false.
    def deslogar
        @logado = false
    end
end

class Aluno < Usuario
    attr_accessor :matricula, :periodo_letivo, :curso, :turmas
    def initialize(email, senha, nome, nascimento, logado, matricula, periodo_letivo, curso, turmas)
        super(email: email, senha: senha, nome: nome, nascimento: nascimento, logado: logado)
        @matricula = matricula
        @periodo_letivo = periodo_letivo
        @curso = curso
        @turma = []
    end

    #Adiciona a turma do aluno no array de turmas.
    def inscrever (nome_turma)
        @turmas.push(nome_turma)
    end

end


class Turma
    attr_accessor :nome, :horario, :dias_da_semana, :inscritos, :inscricao_aberta 
    def initialize(nome, horario, dias_da_semana, inscritos, inscricao_aberta)
        @nome = nome
        @horario = horario
        @dias_da_semana = dias_da_semana
        @inscritos = []
        @inscricao_aberta = inscricao_aberta
    #Modifica a variável inscrição_aberta para true
    def abrir_inscricao
        @inscricao_aberta = true
    end

    #Modifica a variável inscrição_aberta para false
    def fechar_incricao
        @inscricao_aberta = false
    end

    #Adiciona um aluno no array de alunos
    def adicionar_aluno(nome_aluno)
        @inscritos.push (nome_aluno)
    end

end

class Professor < Usuario
   attr_accessor :matricula, :salario, :materias
    def initialize(email, senha, nome, nascimento, logado, matricula, salario, materias)
        super(email: email, senha: senha, nome: nome, nascimento: nascimento, logado: logado, matricula: matricula, salario: salario, materias: materias)
        @matricula = matricula
        @salario = salario
        @materias = []
    end
   #Adiciona nome_materia ao array de materias.
   def adicionar_materia(nome_materia)
        @materias.push(nome_materia)
   end
end

class Materia
    attr_accessor :ementa, :nome, :professores
    def initialize(ementa, nome, professores)
        @ementa = ementa
        @nome = nome
        @professores = []
    end
    #Adiciona professor ao array de professores.
    def adicionar_professor(nome_professor)
        @professores.push(nome_professor)
    end

end
